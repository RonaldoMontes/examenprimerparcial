package com.example.notificacionesupt;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.notificacionesupt.Api.Api;
import com.example.notificacionesupt.Api.Servicios.ServicioPeticion;
import com.example.notificacionesupt.ViewModels.Peticion_Detalle;
import com.example.notificacionesupt.ViewModels.Peticion_Mostrar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Mostrando extends AppCompatActivity {
    private EditText elid, lacaja;
    private Button bac, most;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mostrando);
        elid = (EditText) findViewById(R.id.editText7);
        lacaja = (EditText) findViewById(R.id.editText8);
        bac = (Button) findViewById(R.id.button7);
        most = (Button) findViewById(R.id.button8);

        bac.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Mostrando.this, Menu.class);

                startActivity(intent);
            }
        });
       most.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               ServicioPeticion s = Api.getApi(Mostrando.this).create(ServicioPeticion.class);
               int ids = Integer.parseInt(elid.getText().toString()) ;
               Call<Peticion_Mostrar> Detalle = s.getDetalle(ids);
               Detalle.enqueue(new Callback<Peticion_Mostrar>() {
                   @Override
                   public void onResponse(Call<Peticion_Mostrar> call, Response<Peticion_Mostrar> response) {
                       Peticion_Mostrar mostrar = response.body();
                       String Cadena = "";
                       if (mostrar.estado.equals("true")){
                           for (Noti elemento: mostrar.procesador
                           ) {
                               Cadena = Cadena + elemento.id + " | " + elemento.nombre + " | " + elemento.nucleos + " | " + elemento.frecuencia + "\n";
                               lacaja.setText(Cadena);
                           }




                       }else{
                           Toast.makeText(Mostrando.this, "Datos Incorrectos", Toast.LENGTH_SHORT).show();
                       }
                   }

                   @Override
                   public void onFailure(Call<Peticion_Mostrar> call, Throwable t) {
                       Toast.makeText(Mostrando.this, "Error", Toast.LENGTH_SHORT).show();
                   }
               });
           }
       });
    }
}
